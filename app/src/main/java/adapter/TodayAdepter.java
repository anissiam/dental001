package adapter;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dental.anisandmahmmoud.dentalaandm.Profile_Patient_Activity;
import com.dental.anisandmahmmoud.dentalaandm.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import de.hdodenhof.circleimageview.CircleImageView;
import model.Patient;
import model.vistPatient;
import utils.AppService;

import static android.content.Context.MODE_PRIVATE;

public class TodayAdepter extends RecyclerView.Adapter<TodayAdepter.TodayHolder> {
    FirebaseAuth mAuth;
    Context _cxt;
    private ArrayList<vistPatient> List_vist;
    private FirebaseFirestore firestoreDB;
    private Date date;
    private Calendar date1;
    private String patientId,doctorId,problemId,visitId_Primary,patientPhone;
    public TodayAdepter(Context _cxt, ArrayList<vistPatient> List_vist) {
        this._cxt = _cxt;
        this.List_vist = List_vist;
    }

    @NonNull
    @Override
    public TodayHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_today, parent, false);
        TodayAdepter.TodayHolder holder = new TodayAdepter.TodayHolder(row);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final TodayHolder holder, int position) {
        // mAuth = FirebaseAuth.getInstance();
        // FirebaseUser user = mAuth.getCurrentUser();
        //  Patient patient = list_patient.get(position);
        SharedPreferences prefs = _cxt.getSharedPreferences(AppService.appkey, MODE_PRIVATE);
        final String token = prefs.getString("token", "");

        final vistPatient item = List_vist.get(position);

         date = item.getVisitDate();

        // SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmssSS");
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("d-MM-yyyy");
        String filename = timeStampFormat.format(date);

        Date tday = new Date();
        SimpleDateFormat timeStampFormat1 = new SimpleDateFormat("d-MM-yyyy");
        String filename1 = timeStampFormat.format(date);


        //  Toast.makeText(_cxt, filename, Toast.LENGTH_SHORT).show();


        //Toast.makeText(_cxt, item.getDocumentid(), Toast.LENGTH_SHORT).show();
        holder.tv_name.setText(item.getPatentName());
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(_cxt, Profile_Patient_Activity.class);
                intent.putExtra("id", item.getDocumentid());
                _cxt.startActivity(intent);
            }
        });


        firestoreDB = FirebaseFirestore.getInstance();
        holder.tv_name.setText(item.getPatentName());
        firestoreDB.collection("VisitList").document(item.getDocumentid())
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    String TAG = "Patient";
                    if (document != null && document.exists()) {
                        // String patientName = document.get("patientName").toString();
                         doctorId = document.get("doctorId").toString();
                         patientId = document.get("patientId").toString();
                         problemId = document.get("problemId").toString();
                         visitId_Primary = document.get("visitId_Primary").toString();
                         patientPhone = document.get("patientPhone").toString();
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    String TAG = "Patient";
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });

        firestoreDB
                .collection("Users")
                .document(token)
                .collection("Patient")
                .document(patientId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    String TAG = "Patient";
                    if (document != null && document.exists()) {
                        String patientٍSex_ed = document.get("patientٍSex").toString();

                        if (patientٍSex_ed.equals("ذكر")) {
                            holder.img_profile1.setImageDrawable(_cxt.getResources().getDrawable(R.drawable.male));

                        } else {
                            holder.img_profile1.setImageDrawable(_cxt.getResources().getDrawable(R.drawable.female));

                        }

                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    String TAG = "Patient";
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });


        holder.change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar currentDate = Calendar.getInstance();
                date1 = Calendar.getInstance();
                new DatePickerDialog(_cxt, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        date1.set(year, monthOfYear, dayOfMonth);
                        new TimePickerDialog(_cxt, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                date1.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                date1.set(Calendar.MINUTE, minute);
                                date = date1.getTime();
                                Log.v("time", "The choosen one " + date1.getTime());
                                firestoreDB.collection("Users").document(doctorId).collection("Patient")
                                        .document(patientId).collection("Problem").document(problemId)
                                        .collection("visitPatient").document(visitId_Primary)
                                        .update("visitDate",date1.getTime())
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                firestoreDB.collection("VisitList").document(item.getDocumentid())
                                                        .update("visitDate",date1.getTime())
                                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                            @Override
                                                            public void onSuccess(Void aVoid) {
                                                                sendNotification();
                                                                Toast.makeText(_cxt,"تم تأجيل الزيارة الى "+ date1.getTime(), Toast.LENGTH_SHORT).show();
                                                            }
                                                        });
                                            }
                                        });
                            }
                        }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
                    }
                }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();

            }
        });




        holder.vm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(_cxt, Profile_Patient_Activity.class);
                intent.putExtra("id", item.getDocumentid());
                _cxt.startActivity(intent);


            }
        });


//        holder.btn_row_visit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                /*SharedPreferences.Editor editor = getSharedPreferences(PalcoreStoreService.appkey, MODE_PRIVATE).edit();
//                editor.putString("token1", item.getDocumentid());
//                editor.apply();*/
//
//                Intent intent = new Intent(_cxt,ListVisitActivity.class);
//                intent.putExtra("id",item.getDocumentid());
//               /* Intent intent1 = new Intent(_cxt,VisitActivity.class);
//                intent.putExtra("id1",item.getDocumentid());*/
//             //  Toast.makeText(_cxt, item.getDocumentid(), Toast.LENGTH_SHORT).show();
//                _cxt.startActivity(intent);
//            }
//        });
    }
    /*public void showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        date1 = Calendar.getInstance();
        new DatePickerDialog(_cxt, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date1.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(_cxt, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date1.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date1.set(Calendar.MINUTE, minute);
                        Log.v("time", "The choosen one " + date1.getTime());
                        date = date1.getTime();
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }

        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();

    }*/
    private void sendNotification() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                int SDK_INT = android.os.Build.VERSION.SDK_INT;
                if (SDK_INT > 8) {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                            .permitAll().build();

                    StrictMode.setThreadPolicy(policy);
                    String send_email = patientPhone;
                    /*String Doc_id = "anis.siam.cs@gmail.com";
                    //This is a Simple Logic to Send Notification different Device Programmatically....
                    if (Doc_id.equals("user1@gmail.com")) {
                        send_email = "anis.siam@hotmail.com";
                    } else {
                        send_email = "anis.siam.cs@gmail.com";
                    }*/
                    try {
                        String jsonResponse;

                        URL url = new URL("https://onesignal.com/api/v1/notifications");
                        HttpURLConnection con = (HttpURLConnection) url.openConnection();
                        con.setUseCaches(false);
                        con.setDoOutput(true);
                        con.setDoInput(true);

                        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        con.setRequestProperty("Authorization", "Basic MzZiOTY5NGMtMDU1Ny00NTFhLTllN2MtN2YzMTA4MGFjOTc4");
                        con.setRequestMethod("POST");

                        String strJsonBody = "{"
                                + "\"app_id\": \"ce26d617-0d2a-461d-8be3-9d1d46a6e4af\","

                                + "\"filters\": [{\"field\": \"tag\", \"key\": \"Patient_Id\", \"relation\": \"=\", \"value\": \"" + send_email + "\"}],"

                                + "\"data\": {\"foo\": \"bar\"},"
                                + "\"contents\": {\"en\": \"  تم تغير تأجيل موعد الى $date\"}"
                                + "}";

                        System.out.println("strJsonBody:\n" + strJsonBody);

                        byte[] sendBytes = strJsonBody.getBytes("UTF-8");
                        con.setFixedLengthStreamingMode(sendBytes.length);

                        OutputStream outputStream = con.getOutputStream();
                        outputStream.write(sendBytes);

                        int httpResponse = con.getResponseCode();
                        System.out.println("httpResponse: " + httpResponse);

                        if (httpResponse >= HttpURLConnection.HTTP_OK
                                && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
                            Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
                            jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                            scanner.close();
                        } else {
                            Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
                            jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                            scanner.close();
                        }
                        System.out.println("jsonResponse:\n" + jsonResponse);

                    } catch (Throwable t) {
                        t.printStackTrace();
                    }
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return List_vist.size();
    }

    public class TodayHolder extends RecyclerView.ViewHolder {
        private   TextView tv_name;
        Button btn_row_visit;
        private RelativeLayout relative_data;
        private   CircleImageView img_profile1;
        private View vm;
        private Button change;
        public TodayHolder(View view){
            super(view);
            vm = view;
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            img_profile1 = (CircleImageView) view.findViewById(R.id.img_profile1);
            change = (Button) view.findViewById(R.id.change);
//            btn_row_visit = (Button) view.findViewById(R.id.btn_row_visit);
            relative_data = (RelativeLayout) view.findViewById(R.id.relative_data);
        }
    }
    public void setFilter(ArrayList<vistPatient> newlist_visit) {
        List_vist = new ArrayList<>();
        List_vist.addAll(newlist_visit);
        notifyDataSetChanged();
    }
}

